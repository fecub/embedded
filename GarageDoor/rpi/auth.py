from flask import flash, Blueprint, render_template, redirect, url_for, request
from werkzeug.security import check_password_hash
from flask_login import login_user, logout_user, login_required
from . import db
from flask_login import login_user
from .models import User

auth = Blueprint('auth', __name__)

@auth.route('/login')
def login():
    return render_template('login.html')

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))

@auth.route('/login', methods=['POST'])
def login_post():
    username = request.form.get('username')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(username=username).first()

    # check if user actually exists
    # take the user supplied password, hash it, and compare it to the hashed password in database
    if not user or not check_password_hash(user.password, password): 
        flash('Benutzername oder Password ungültig.')
        return redirect(url_for('main.index')) # if user doesn't exist or password is wrong, reload the page


    login_user(user, remember=remember)
    return redirect(url_for('main.index'))