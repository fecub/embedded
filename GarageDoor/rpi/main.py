from flask import flash, Blueprint, render_template, redirect, url_for
from flask_login import login_required, current_user
from . import db
from . import control
import time 

main = Blueprint('main', __name__)

@main.route('/')
@login_required
def index():
    return render_template('index.html')


@main.route('/opendoor')
@login_required
def opendoor():
    flash('TOR REAGIERT!')
    flash('HINWEIS: Wenn in 10 Sekunden das Tor nicht reagiert, bitte nochmal klicken')
    control()
    return render_template('index_info.html')
    
    # return redirect(url_for('main.index'))

# @main.route('/open')
# @login_required
# def open():
#     return render_template('open.html', name=current_user.username)
