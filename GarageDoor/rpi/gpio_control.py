import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

RELAIS_1_GPIO = 17
def control():
    GPIO.setup(RELAIS_1_GPIO, GPIO.OUT)
    GPIO.output(RELAIS_1_GPIO, GPIO.LOW)
    GPIO.output(RELAIS_1_GPIO, GPIO.HIGH)
    time.sleep(0.6)
    GPIO.output(RELAIS_1_GPIO, GPIO.LOW)

    
