#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include "htmlpage.h"

#ifndef STASSID
#define STASSID "Fritzbox!KF"
#define STAPSK  "a7a8588a8ex58"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

ESP8266WebServer server(80);

const char* www_username = "username";
const char* www_password = "password";


void openCloseDoor();

int LEDPIN = 2;
int PINSTATUS = false;
int RELAYPIN = 5;

void setup() {
    #pragma region BASICS DEFINITION
    // Serial.begin(9600);
    
    // pin modes for gpios
    // pinMode(LEDPIN, OUTPUT);
    pinMode(RELAYPIN, OUTPUT);
    // digitalWrite(LEDPIN, HIGH);
    digitalWrite(RELAYPIN, LOW);

    // set as access point 
    WiFi.softAP(ssid, password); //begin WiFi access point
    
    // authentication 
    ArduinoOTA.begin();

    delay(1000);

    #pragma endregion

    #pragma region ROUTES
    server.on("/", []() {
        if (!server.authenticate(www_username, www_password)) {
            return server.requestAuthentication();
        }
        server.send(200, "text/html", mainpage);
        delay(1000);
    });

    server.on("/do", []() {
        if (!server.authenticate(www_username, www_password)) {
            return server.requestAuthentication();
        }
        server.send(200, "text/html", mainpage);
        Serial.print(PINSTATUS);
        if (PINSTATUS == false) {
            // digitalWrite(LEDPIN, LOW );
            openCloseDoor();
            PINSTATUS = true;
        }
        else {
            // digitalWrite(LEDPIN, HIGH );
            openCloseDoor();
            PINSTATUS = false;
        }

        delay(1000);
    });


    server.begin();

    #pragma endregion

    #pragma region DEBUG
    // Serial.print("Open http://");
    // Serial.print(WiFi.localIP());
    // Serial.println("/ in your browser to see it working");
    #pragma endregion
}

void loop() {
    ArduinoOTA.handle();
    server.handleClient();
}

#pragma region METHODS

void openCloseDoor() {
    digitalWrite(RELAYPIN, HIGH );
    delay(600);
    digitalWrite(RELAYPIN, LOW );
}

#pragma endregion