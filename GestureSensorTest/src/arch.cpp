#include <Arduino.h>
#include "arch.h"

#pragma region LedController
Arch::LedController::LedController() {
    FastLED.addLeds<CHIPSET, LED_PIN, GRB>(leds, NUM_LEDS);
}

void Arch::LedController::blink_on(int dot, int r, int g, int b, bool isStepping) {
    for (int i=255; i>=0; i=i-25) {
        leds[dot] = CRGB( r, g, b);
        leds[dot].fadeLightBy(i);
        if(isStepping)
            FastLED.show();
    }
    if(!isStepping)
        FastLED.show();
}

void Arch::LedController::blink_off(int dot) {
    leds[dot] = CRGB::Black;
    FastLED.show();
}

void Arch::LedController::clear() {
    FastLED.clear();
}
#pragma endregion

#pragma region Line_Arch
void Arch::Line_Arch::init() {
}

void Arch::Line_Arch::blink_half_top(int r=255, int g=255, int b=255) {
    Arch::Line_Arch::clear_all();
    for (int i = NUM_LEDS / 2; i<=NUM_LEDS;i++) {
        Serial.println(i);
        blink_on(i,r, g, b);
    }
}

void Arch::Line_Arch::blink_half_down(int r=255, int g=255, int b=255) {
    Arch::Line_Arch::clear_all();
    for (int i = NUM_LEDS / 2; i>=0; i--) {
        Serial.println(i);
        blink_on(i,r, g, b);
    }
}

void Arch::Line_Arch::blink_all_led_blue(int r, int g, int b) {
    Arch::Line_Arch::clear_all();
    for (int i = 0; i<NUM_LEDS;i++)
        blink_on(i,r, g, b, false);
}

void Arch::Line_Arch::blink_all_led_green(int r, int g, int b) {
    Arch::Line_Arch::clear_all();
    for (int i = 0; i<NUM_LEDS;i++)
        blink_on(i,r, g, b, false);
}

void Arch::Line_Arch::turn_on_led(int r, int g, int b) {
    Arch::Line_Arch::clear_all();
    for (int i = 0; i<NUM_LEDS;i++)
        blink_on(i,r, g, b, false);
}

void Arch::Line_Arch::turn_off_all() {
    Arch::Line_Arch::clear_all();
    for (int i = 0; i<NUM_LEDS;i++)
        blink_off(i);
}

void Arch::Line_Arch::clear_all() {
    clear();
}

#pragma endregion //Line_Arch
