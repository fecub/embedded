#ifndef ARCH_H
#define ARCH_H

#include <Arduino.h>
#include <FastLED.h>

#define NUM_ROWS 3
#define NUM_COLS 4

// #define COLOR_ORDER GRB
#define CHIPSET     WS2812B
#define LED_PIN     13
#define NUM_LEDS    33
#define BRIGHTNESS  10


namespace Arch {

    #pragma region BASIS
    class LedController {
    public:
        LedController();

    protected:
        void blink_on(int dot, int r, int g, int b, bool isStepping=true);
        void blink_off(int dot);

        void clear();

    private:
        CRGB leds[NUM_LEDS];
    };
    #pragma endregion //BASIS

    #pragma region LedController
    class Line_Arch : LedController {
    public:
        void init();
        void blink_half_top(int r=255, int g=255, int b=255);
        void blink_half_down(int r=255, int g=255, int b=255);
        void blink_all_led_blue(int r=255, int g=255, int b=255);
        void blink_all_led_green(int r=255, int g=255, int b=255);
        void turn_on_led(int r=255, int g=255, int b=255);
        void turn_off_all();
        void clear_all();
    private:
    
    };
    #pragma endregion //LedController
}

#endif // ARCH_H