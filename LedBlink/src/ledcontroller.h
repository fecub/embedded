#ifndef LEDCONTROLLER_H
#define LEDCONTROLLER_H

#include <Arduino.h>
#include <FastLED.h>

// #define COLOR_ORDER GRB
#define CHIPSET     WS2812B
#define LED_PIN     13
#define NUM_LEDS    33
#define BRIGHTNESS  100

class LedController{
public: 
    LedController();
    void move();
    void moveBlock();
    void moveSlightly();

    void showBlock(int row);

    void disable();

private:
    CRGB leds[NUM_LEDS];


};

#endif // LEDCONTROLLER_H