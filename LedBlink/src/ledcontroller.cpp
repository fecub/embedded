#include "ledcontroller.h"
#include <Arduino.h>

LedController::LedController() {
    FastLED.addLeds<CHIPSET, LED_PIN>(leds, NUM_LEDS);
    
}

void LedController::move() {
    for(int dot = 0; dot < NUM_LEDS; dot++) { 
        leds[dot] = CRGB::Blue;
        FastLED.show();
        delay(100);                
        // clear this led for the next time around the loop
        leds[dot] = CRGB::Black;
        FastLED.show();
        delay(50);
    }
    for(int dotback = 32; dotback >= 0; --dotback) { 
        leds[dotback] = CRGB::Blue;
        FastLED.show();
        delay(100);        
        // clear this led for the next time around the loop
        leds[dotback] = CRGB::Black;
        FastLED.show();
        delay(50);
    }
}

void LedController::moveBlock() {
    for(int dot = 0; dot < NUM_LEDS; dot++) { 
        leds[dot] = CRGB::Blue;
        leds[dot-1] = CRGB::Blue;
        leds[dot-2] = CRGB::Blue;
        FastLED.show();
        delay(50);                
        // clear this led for the next time around the loop
        leds[dot] = CRGB::Black;
        leds[dot-1] = CRGB::Black;
        leds[dot-2] = CRGB::Black;
        FastLED.show();
        delay(10);
    }
    for(int dotback = 29; dotback > 0; --dotback) { 
        leds[dotback] = CRGB::Blue;
        leds[dotback+1] = CRGB::Blue;
        leds[dotback+2] = CRGB::Blue;
        FastLED.show();
        delay(50);                
        // clear this led for the next time around the loop
        leds[dotback] = CRGB::Black;
        leds[dotback+1] = CRGB::Black;
        leds[dotback+2] = CRGB::Black;
        FastLED.show();
        delay(10);
    }
}

void LedController::moveSlightly() {
    for(int dot=0; dot < NUM_LEDS; dot++) {
        FastLED.clear();
        // leds[dot] = CRGB::Red;
        leds[dot].g = 5;
        leds[dot+1].g = 20;
        leds[dot+2].g = 60;
        leds[dot+3].g = 80;
        leds[dot+4].g = 250;
        FastLED.show();
        delay(100);
    }
}

void LedController::showBlock(int row) {
    // leds[0].g
}

void LedController::disable() {
    FastLED.clear();
}