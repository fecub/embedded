# Collection of small Projects for ESP8266, Raspberry Pi & Co
 
- GarageDoor
    - Automatic garage door opener
- GestureSensorTest
    - https://www.instagram.com/p/BhjFV2ZgCRC/
- LedBlink
    - some WS2818b let testing
- LedCatcherReloaded
    - toy for my on going hacker 
    - https://www.instagram.com/p/BiAn-ezAtsZ/
- XYMatrix 