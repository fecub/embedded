#include <Arduino.h>
#include "arch.h"

#pragma region LedController
Arch::LedController::LedController() {
    FastLED.addLeds<CHIPSET, LED_PIN, GRB>(leds, NUM_LEDS);
}

void Arch::LedController::blink_on(int dot, int r, int g, int b) {
    for (int i=255; i>=0; i=i-9) {
    // leds[dot] = CHSV( r, g, b);
        leds[dot] = CRGB( r, g, b);
        leds[dot].fadeLightBy(i);
        FastLED.show();
    }
}

void Arch::LedController::blink_off(int dot) {
    leds[dot] = CRGB::Black;
    FastLED.show();
}

void Arch::LedController::clear() {
    FastLED.clear();
}
#pragma endregion


#pragma region Matrix_Arch
void Arch::Matrix_Arch::init() {
    int i=0;
    for(int row = 0; row < NUM_ROWS; row++) {
        for(int col = 0; col < NUM_COLS; col++) {
            XY_Matrix_Arch[row][col] = i;
            i++;
        }
    }
}

void Arch::Matrix_Arch::blink_xy_on(int row, int col, int r, int g, int b) {
    blink_on(XY_Matrix_Arch[row][col], r, g, b);
}
void Arch::Matrix_Arch::blink_xy_off(int row, int col) {
    blink_off(XY_Matrix_Arch[row][col]);
}

void Arch::Matrix_Arch::blink_block_on(int row, int r, int g, int b) {
    blink_on(XY_Matrix_Arch[row][0],r, g, b);
    blink_on(XY_Matrix_Arch[row][1],r, g, b);
    blink_on(XY_Matrix_Arch[row][2],r, g, b);
    blink_on(XY_Matrix_Arch[row][3],r, g, b);
}

void Arch::Matrix_Arch::blink_block_off(int row) {
    blink_off(XY_Matrix_Arch[row][0]);
    blink_off(XY_Matrix_Arch[row][1]);
    blink_off(XY_Matrix_Arch[row][2]);
    blink_off(XY_Matrix_Arch[row][3]);
}

void Arch::Matrix_Arch::clear_all() {
    clear();
}

#pragma endregion
