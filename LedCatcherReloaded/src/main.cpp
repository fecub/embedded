#include <Arduino.h>
#include "arch.h"

Arch::Matrix_Arch xyma;
bool checkledchanged = false;
// xyma.blink_block(2, 250,0,0);
// xyma.blink_block(1, 0,0,250);
// xyma.blink_block(0, 236,197,0);

// DEFINES
// #define B1COLOR 255,0,0
// #define B2COLOR 255,255,255
// #define B3COLOR 0,255,0

#define B1COLOR 255,255,255
#define B2COLOR 255,0,0
#define B3COLOR 255,255,255

// VARIABLES
const int BUTTON_MODE_PIN = 3;
int button_mode = 0;

const int buzzer_pin = 2;

const int button_1 = 12;
const int button_2 = 11;
const int button_3 = 10;

int button_row_1;
int button_row_2;
int button_row_3;



// DEFINITION
void listenModeChange();
void click();
void play();
void demo();
void checkButtonLed(int BUTTON, int ROW);
int random_led();
void clear_led();
// void beep(int tone_type);
// void beep(int tone_type, bool manual_ss);

// MODES
// CLICK=1, PLAYING=2, DEMO=3
int mode=1;

// PLAYING Variables
bool asked = false;
int led_pin_random;
int checkValue;



void setup() {
    // put your setup code here, to run once:
    Serial.begin(9600);
    xyma.init();
}

void loop() {
    // put your main code here, to run repeatedly:
    // listenModeChange();
    if (mode == 1) {
        // CLICK
        click();
    }
    else if (mode == 2) {
        // // PLAYING  
        play();

    }
    else if (mode == 3) {
        // // DEMO  
        demo();

    }
}

void listenModeChange() {
    button_mode = digitalRead(BUTTON_MODE_PIN);
    if (button_mode == HIGH) {
        if (mode<3) {
            mode += 1;
            if (mode == 2) {
                for (int i = 0; i<2; i++) {
                    xyma.blink_block_on(0, B1COLOR);
                    xyma.blink_block_on(1, B2COLOR);
                    xyma.blink_block_on(2, B3COLOR);
                    delay(500);
                    xyma.blink_block_off(0);
                    xyma.blink_block_off(1);
                    xyma.blink_block_off(2);
                    delay(500);
                    // beep(1);
                }
            }
            if (mode == 3) {
                for (int i = 0; i<3; i++) {
                    xyma.blink_block_on(0, B1COLOR);
                    xyma.blink_block_on(1, B2COLOR);
                    xyma.blink_block_on(2, B3COLOR);
                    delay(500);
                    xyma.blink_block_off(0);
                    xyma.blink_block_off(1);
                    xyma.blink_block_off(2);
                    delay(500);
                    // beep(1);
                }
            }
        }
        else {
            mode = 1;
            xyma.blink_block_on(0, B1COLOR);
            xyma.blink_block_on(1, B2COLOR);
            xyma.blink_block_on(2, B3COLOR);
            delay(500);
            xyma.blink_block_off(0);
            xyma.blink_block_off(1);
            xyma.blink_block_off(2);
            // beep(1);
        }
        delay(50);
    }
}


void click() {
    checkButtonLed(button_1, 0);
    checkButtonLed(button_2, 1);
    checkButtonLed(button_3, 2);
}

void play() {
    if (!asked) {
        xyma.clear_all();
        led_pin_random = random_led();
        if (led_pin_random == 1)
            xyma.blink_block_on(0, B1COLOR);
        if (led_pin_random == 2)
            xyma.blink_block_on(1, B2COLOR);
        if (led_pin_random == 3)
            xyma.blink_block_on(2, B3COLOR);

        asked = true;
    }
    else {
        // check right click
        button_row_1 = digitalRead(button_1);
        button_row_2 = digitalRead(button_2);
        button_row_3 = digitalRead(button_3);
        
        if (button_row_1 == HIGH)
            checkValue = 1;
        if (button_row_2 == HIGH)
            checkValue = 2;
        if (button_row_3 == HIGH)
            checkValue = 3;

        // Serial.println(checkValue);
        // Serial.println(led_pin_random);
        if (checkValue == led_pin_random) {
            // beep(2);
            if (led_pin_random == 1)
                xyma.blink_block_on(0, B1COLOR);
                // digitalWrite(led_pin_1, LOW);
            if (led_pin_random == 2)
                xyma.blink_block_on(1, B2COLOR);
                // digitalWrite(led_pin_2, LOW);
            if (led_pin_random == 3)
                xyma.blink_block_on(2, B3COLOR);
                // digitalWrite(led_pin_3, LOW);
            checkValue=0;
            asked = false;
        }
        else if(checkValue != 0) {
            // beep(3);
            checkValue = 0;
        }
    }
    delay(50);
}

void demo() {
    xyma.clear_all();
    // beep(1, false);
    led_pin_random = random_led();
    Serial.println(led_pin_random);
    if (led_pin_random == 1){
        xyma.blink_block_on(0, B1COLOR);
        // beep(3, true);
    }
    if (led_pin_random == 2) {
        xyma.blink_block_on(1, B2COLOR);
        // beep(2, true);
    }
    if (led_pin_random == 3){
        xyma.blink_block_on(2, B3COLOR);
        // beep(1, true);
    }

    delay(random(500, 1500));

}

void checkButtonLed(int BUTTON, int ROW) {
    int button = digitalRead(BUTTON);
    
    if (button == HIGH) {
        xyma.clear_all();
        if (ROW == 0)
            xyma.blink_block_on(ROW, B1COLOR);
        if (ROW == 1)
            xyma.blink_block_on(ROW, B2COLOR);
        if (ROW == 2)
            xyma.blink_block_on(ROW, B3COLOR);

        
        // beep(2, true);
    }
    // delay(1000);
}

int random_led() {
    delay(100);
    int randnumber;
    randnumber = random(1, 4);
    return randnumber;
}