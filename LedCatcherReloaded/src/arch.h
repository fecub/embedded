#ifndef ARCH_H
#define ARCH_H

#include <Arduino.h>
#include <FastLED.h>

#define NUM_ROWS 3
#define NUM_COLS 4

// #define COLOR_ORDER GRB
#define CHIPSET     WS2812B
#define LED_PIN     13
#define NUM_LEDS    33
#define BRIGHTNESS  10


namespace Arch {

    class LedController{
    public:
        LedController();

    protected:
        void blink_on(int dot, int r, int g, int b);
        void blink_off(int dot);

        void clear();

    private:
        CRGB leds[NUM_LEDS];
    };

    class Matrix_Arch : LedController {
    public:
        void init();
        void blink_xy_on(int row, int col, int r=255, int g=255, int b=255);
        void blink_xy_off(int row, int col);
        void blink_block_on(int row,  int r=255, int g=255, int b=255);
        void blink_block_off(int row);
        void clear_all();
        
    private: 
        int XY_Matrix_Arch[NUM_ROWS][NUM_COLS];
    };
}

#endif // ARCH_H